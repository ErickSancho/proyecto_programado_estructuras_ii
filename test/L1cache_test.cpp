/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>



class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) { 
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on, checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 1;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
  int status;
  int i;
  int idx;
  int A_Tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool debug = 0;
  operation_result result = {};
  int rand_policy = (rand()%3); // 0->lru, 1->nru, 2->srrip

  /* Fill a random cache entry */
  idx = rand()%1024;
  A_Tag = rand()%4096;
  associativity = 1 << (rand()%4);
  switch (rand_policy)
  {
  case LRU: //LRU
    printf(CYN "INFO: " RESET "LRU policy\n\n");
    break;
  case NRU: //NRU
    printf(CYN "INFO: " RESET "NRU policy\n\n");
    break;
  case RRIP: //SRRIP
    printf(CYN "INFO: " RESET "SRRIP policy\n\n");
    break;
  default:
    break;
  }

  if (debug_on) {
    printf("Black A Info\n idx: %d\n A_Tag: %d\n associativity: %d\n",
          idx,
          A_Tag,
          associativity);
  }

  // Se genera un entry_info a partir de access
  entry cache_line[associativity];
  // estructura results
  struct operation_result results = {};

  //Defino un loadstore aleatorio
  int loadstore = rand()%2;

  // Posicion del bloque A
  int pos_A;
  if (associativity<=1)
  {
    pos_A = associativity-1;
  }
  else
  {
    pos_A = (associativity%2) ? (associativity+1)/2 : (associativity)/2; 
  }

  
  // Caso de tener politica LRU
  if(rand_policy == LRU)
  {
    // Llenamos el cache_entry, he insertamos el bloque A
    for (int i =  0; i < associativity; i++) {
      if (i==pos_A)
      {
        // printf("Cargamos el bloque A en la posicion: %d\n", pos_A);
        cache_line[i].valid = true;
        cache_line[i].tag = A_Tag;
        cache_line[i].dirty = 0;
        cache_line[i].rp_value = i;
      }
      else
      {
        cache_line[i].valid = true;
        cache_line[i].tag = rand()%4096;
        cache_line[i].dirty = 0;
        cache_line[i].rp_value = i;
        while(cache_line[i].tag==A_Tag)
        {
          cache_line[i].tag = rand()%4096;
        }
      }      
    }

    if(debug_on)
    {
      printf("Tag de A: %d, Pos_A: %d, associativity: %d\n", A_Tag, pos_A, associativity);
      printf("Cargamos el bloque A en la posicion: %d\n", pos_A);
    }

    // Force a hit
    DEBUG(debug_on, Checking block A hit operation);
    int status = lru_replacement_policy(idx, A_Tag, associativity, loadstore, cache_line, &results, debug_on);
    EXPECT_EQ(status, 0);
    int miss_hit = (loadstore) ? HIT_STORE : HIT_LOAD;
    EXPECT_EQ(results.miss_hit, miss_hit);

    // Check rp_value of block A
    EXPECT_EQ(cache_line[pos_A].rp_value, associativity-1);

    // Keep inserting new bloack until A is evicted
    for (int i =  0; i < associativity; i++) 
    {
      int Ntag = rand()%4096;
      while(is_in_set(cache_line, associativity, Ntag))
      {
        Ntag = rand()%4096;
      }
      loadstore = rand()%2;
      int status = lru_replacement_policy(idx, Ntag, associativity, loadstore, cache_line, &results, debug_on);
      EXPECT_EQ(status, 0);

      // if(debug_on)
      // {
      //   printf("Evicted address: %d\n", results.miss_hit);
      // }
    }      
    
    // Eviction of block A
    DEBUG(debug_on, Eviction of block A);
    int miss_hit_status = (loadstore) ? MISS_STORE : MISS_LOAD;
    EXPECT_EQ(results.miss_hit, miss_hit_status);
    EXPECT_EQ(results.evicted_address, A_Tag);   
  }


  ///////////////////////////////////////////
  // Caso de tener politica NRU
  if(rand_policy == NRU)
  {
    // Llenamos el cache_entry, he insertamos el bloque A
    for (int i =  0; i < associativity; i++) {
      if (i==pos_A)
      {
        // printf("Cargamos el bloque A en la posicion: %d\n", pos_A);
        cache_line[i].valid = true;
        cache_line[i].tag = A_Tag;
        cache_line[i].dirty = false;
        cache_line[i].rp_value = 0;
      }
      else
      {
        cache_line[i].valid = true;
        cache_line[i].tag = rand()%4096;
        cache_line[i].dirty = false;
        cache_line[i].rp_value = 0;
        while(cache_line[i].tag==A_Tag)
        {
          cache_line[i].tag = rand()%4096;
        }
      }      
    }

    if(debug_on)
    {
      printf("Tag de A: %d, Pos_A: %d, associativity: %d\n", A_Tag, pos_A, associativity);
      printf("Cargamos el bloque A en la posicion: %d\n", pos_A);
    }

    // Force a hit
    // DEBUG(debug_on, Checking block A hit operation);
    int status = nru_replacement_policy(idx, A_Tag, associativity, loadstore, cache_line, &results, debug_on);
    EXPECT_EQ(status, 0);
    int miss_hit = (loadstore) ? HIT_STORE : HIT_LOAD;
    EXPECT_EQ(results.miss_hit, miss_hit);
    // Check rp_value of block A
    EXPECT_EQ(cache_line[pos_A].rp_value, 0);

    // if(debug_on)
    // {
    //   printf("1->Tag de la posicion de A: %d\n", cache_line[pos_A].tag);
    // }
    // Keep inserting new bloack until A is evicted
    for (int i =  0; i < pos_A+1; i++) 
    {
      int Ntag = rand()%4096;
      while(is_in_set(cache_line, associativity, Ntag))
      {
        Ntag = rand()%4096;
      }
      // if(debug_on)
      // {
      // printf("%d->Tag de la posicion de A: %d, y un rp_value: %d\n", i, cache_line[pos_A].tag, cache_line[pos_A].rp_value);
      // }
      loadstore = rand()%2;
      int status = nru_replacement_policy(idx, Ntag, associativity, loadstore, cache_line, &results, debug_on);
      EXPECT_EQ(status, 0);
    }
    // printf("Tag de la posicion de A: %d\n", cache_line[pos_A].tag);
    DEBUG(debug_on, Eviction of block A);
    int miss_hit_status = (loadstore) ? MISS_STORE : MISS_LOAD;
    EXPECT_EQ(results.miss_hit, miss_hit_status);
    EXPECT_EQ(results.evicted_address, A_Tag);
  }


  ///////////////////////////////////////////
  if(rand_policy == RRIP)
  {
    //Definimos el valor de M
    int M, RP;
    if (associativity<=2)
    {
      M=1;
      RP = pow(2, M) - 2;
    }
    else 
    {
      M=2;
      RP=pow(2, M) - 2;
    }

    // Llenamos el cache_entry, he insertamos el bloque A
    for (int i =  0; i < associativity; i++) {
      if (i==pos_A)
      {
        // printf("Cargamos el bloque A en la posicion: %d\n", pos_A);
        cache_line[i].valid = true;
        cache_line[i].tag = A_Tag;
        cache_line[i].dirty = false;
        cache_line[i].rp_value = RP;        
      }
      else
      {
        cache_line[i].valid = true;
        cache_line[i].tag = rand()%4096;
        cache_line[i].dirty = false;
        cache_line[i].rp_value = RP;
        while(cache_line[i].tag==A_Tag)
        {
          cache_line[i].tag = rand()%4096;
        }
      }      
    }

    if(debug_on)
    {
      printf("Tag de A: %d, Pos_A: %d, associativity: %d\n", A_Tag, pos_A, associativity);
      printf("Cargamos el bloque A en la posicion: %d\n", pos_A);
    }

    // Force a hit
    // DEBUG(debug_on, Checking block A hit operation);
    int status = srrip_replacement_policy(idx, A_Tag, associativity, loadstore, cache_line, &results, debug_on);
    EXPECT_EQ(status, 0);
    int miss_hit = (loadstore) ? HIT_STORE : HIT_LOAD;
    EXPECT_EQ(results.miss_hit, miss_hit);
    // Check rp_value of block A
    EXPECT_EQ(cache_line[pos_A].rp_value, 0);

    // if(debug_on)
    // {
    //   printf("1->Tag de la posicion de A: %d\n", cache_line[pos_A].tag);
    // }
    // Keep inserting new bloack until A is evicted
    int cont=1;
    while (results.evicted_address != A_Tag) 
    {
      int Ntag = rand()%4096;
      while(is_in_set(cache_line, associativity, Ntag))
      {
        Ntag = rand()%4096;
      }
      if(debug_on)
      {
        printf("%d->Tag de la posicion de A: %d, y un rp_value: %d\n", cont, cache_line[pos_A].tag, cache_line[pos_A].rp_value);
      }
      loadstore = rand()%2;
      int status = srrip_replacement_policy(idx, Ntag, associativity, loadstore, cache_line, &results, debug_on);
      EXPECT_EQ(status, 0);
      cont++;
    }
    // printf("Tag de la posicion de A: %d\n", cache_line[pos_A].tag);
    DEBUG(debug_on, Eviction of block A);
    int miss_hit_status = (loadstore) ? MISS_STORE : MISS_LOAD;
    EXPECT_EQ(results.miss_hit, miss_hit_status);
    EXPECT_EQ(results.evicted_address, A_Tag);
  }

}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug = 0;
  struct operation_result resultA = {};
  struct operation_result resultB = {};
  /* Choose a random policy */
  int rand_policy = rand()%3;

  /* Choose a random associativity */
  associativity = 1 << (rand()%4);

  /* Fill a cache entry with only read operations */
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_lines[2][associativity];
  for(int i = 0; i < 2; i++){
    for (int j =  0; j < associativity; j++) {
      cache_lines[i][j].valid = true;
      cache_lines[i][j].tag = rand()%4096;
      cache_lines[i][j].dirty = false;
      if (rand_policy == LRU){
        cache_lines[i][j].rp_value = 0;
      } else if(rand_policy == NRU){
        cache_lines[i][j].rp_value = 1;
      } else if(rand_policy == RRIP){
        cache_lines[i][j].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
    }
  }
  /* Create a random block A */
  idx = rand()%1024;
  tag = rand()%4096;
  loadstore = STORE; 
  int tagA = tag;
  switch (rand_policy)
  {
    case LRU:
      lru_replacement_policy (idx,
                              tag,
                              associativity,
                              loadstore,
                              cache_lines[0],
                              &resultA,
                              bool(debug_on));
      break;
    case NRU:
      nru_replacement_policy(idx,
                             tag,
                             associativity,
                             loadstore,
                             cache_lines[0],
                             &resultA,
                             bool(debug_on));
      break;
    case RRIP:
      srrip_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_lines[0],
                               &resultA,
                               bool(debug_on));
      break;
  }

   /* Force a write hit for a random block A */
   /* block A was replaced in last iteration, if we used the same 
   *  tag now we will get a hit
   */
   loadstore = STORE;
   switch (rand_policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_lines[0],
                                &resultA,
                                bool(debug_on));
        break;
      case NRU:
        nru_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_lines[0],
                               &resultA,
                               bool(debug_on));
        break;
      case RRIP:
        srrip_replacement_policy(idx,
                                 tag,
                                 associativity,
                                 loadstore,
                                 cache_lines[0],
                                 &resultA,
                                 bool(debug_on));
        break;
    default:
      break;
    }
  /* Create a random block B */
  idx = rand()%1024;
  int tagB = rand()%4096;
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  loadstore = LOAD;
  tag = tagB;
  switch (rand_policy)
  {
    case LRU:
      lru_replacement_policy (idx,
                              tag,
                              associativity,
                              loadstore,
                              cache_lines[1],
                              &resultB,
                              bool(debug_on));
      break;
    case NRU:
      nru_replacement_policy(idx,
                             tag,
                             associativity,
                             loadstore,
                             cache_lines[1],
                             &resultB,
                             bool(debug_on));
      break;
    case RRIP:
      srrip_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_lines[1],
                               &resultB,
                               bool(debug_on));
      break;
    default:
      break;
  }
  /* Force a read hit for a random block B */
  /* block B was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  switch (rand_policy)
  {
    case LRU:
      lru_replacement_policy (idx,
                              tag,
                              associativity,
                              loadstore,
                              cache_lines[1],
                              &resultB,
                              bool(debug_on));
      break;
    case NRU:
      nru_replacement_policy(idx,
                             tag,
                             associativity,
                             loadstore,
                             cache_lines[1],
                             &resultB,
                             bool(debug_on));
      break;
    case RRIP:
      srrip_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_lines[1],
                               &resultB,
                               bool(debug_on));
      break;
    default:
      break;
  }
  
  /* Force a read hit for a random block A */
  /* block B was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  loadstore = LOAD;
  switch (rand_policy)
  {
    case LRU:
      lru_replacement_policy (idx,
                              tag,
                              associativity,
                              loadstore,
                              cache_lines[0],
                              &resultA,
                              bool(debug_on));
      break;
    case NRU:
      nru_replacement_policy(idx,
                             tag,
                             associativity,
                             loadstore,
                             cache_lines[0],
                             &resultA,
                             bool(debug_on));
      break;
    case RRIP:
      srrip_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_lines[0],
                               &resultA,
                               bool(debug_on));
      break;
  }

 /* Insert lines until B is evicted */

  while(resultB.evicted_address != tagB)
  {
    tag = rand()%4096;
    switch (rand_policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_lines[1],
                                &resultB,
                                bool(debug_on));
      break;
      case NRU:
        nru_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_lines[1],
                               &resultB,
                               bool(debug_on));
      break;
      case RRIP:
        srrip_replacement_policy(idx,
                                 tag,
                                 associativity,
                                 loadstore,
                                 cache_lines[1],
                                 &resultB,
                                 bool(debug_on));
      break;
    default:
      break;
    }
  }

  /* Check dirty_bit for block B is false */
  EXPECT_EQ(resultB.dirty_eviction, false);
  EXPECT_EQ(resultB.evicted_address, tagB);

  /* Insert lines until A is evicted */
  while(resultA.evicted_address != tagA)
  {
    tag = rand()%4096;
    switch (rand_policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_lines[0],
                                &resultA,
                                bool(debug_on));
      break;
      case NRU:
        nru_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_lines[0],
                               &resultA,
                               bool(debug_on));
      break;
      case RRIP:
        srrip_replacement_policy(idx,
                                 tag,
                                 associativity,
                                 loadstore,
                                 cache_lines[0],
                                 &resultA,
                                 bool(debug_on));
      break;
    default:
      break;
    }
  }

    /* Check dirty bit for block A is true */
    EXPECT_EQ(resultA.dirty_eviction, true);
    EXPECT_EQ(resultA.evicted_address, tagA);
}


/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Choose a random policy */
  int rand_policy = rand()%3;

  /* Choose invalid parameters for idx, tag and asociativy */
  idx = -1;
  tag = -1;
  associativity = 2;

  /* Fill a cache entry */
  struct entry cache_line[associativity];
  for ( i =  0; i < associativity+1; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      if(rand_policy == 1){
        cache_line[i].rp_value = i;
      }
      else if(rand_policy == 2){
        cache_line[i].rp_value = 1;
      }
      else if(rand_policy == 3){
        cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
  }

  associativity = associativity*(-1);
  /* Check function returns a PARAM error condition */
  switch (rand_policy)
  {
  case LRU:
    status = lru_replacement_policy(idx,
                                      tag,
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));
    break;                                

  case NRU:
    status = nru_replacement_policy(idx,
                                      tag,
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));
    break;

  case RRIP:
    status = srrip_replacement_policy(idx,
                                      tag,
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));
  break;

  default:
    break;
  }

  /* Check function returns a PARAM error condition */
  EXPECT_EQ(status, 1);

}