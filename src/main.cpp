#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>
#define KB 1024
#define ADDRSIZE 32

using namespace std;

  // Funcion de impresion
void print_usage(int size, int associativity, int block_size, int rp, float load_h, float load_m, float store_h, float store_m, int dirty_evic)
{
  float total_m = load_m + store_m;
  float total_h = load_h + store_h;
  float missrate = (float)total_m/(float)(total_h + total_m);
  cout << "_______________________________________" << endl << "Cache parameters:"<< endl <<"_______________________________________\n" << endl;
  cout << "Cache Size (KB):               "<< size << endl;
  cout << "Cache Associativity:           "<< associativity << endl;
  cout << "Cache Block Size (bytes):      "<< block_size << endl;
  if(rp == LRU){
    cout << "Replacement Policy:            LRU" << endl;
  }
  else if(rp == NRU){
    cout << "Replacement Policy:            NRU" << endl;
  }
  else if(rp == RRIP){
    cout << "Replacement Policy:            SRRIP" << endl;
  }
  cout << "_______________________________________" << endl << "Simulation results:"<< endl <<"_______________________________________\n" << endl;
  cout << "Dirty evictions:               "<< dirty_evic << endl;
  cout << "Load misses:                   "<< load_m << endl;
  cout << "Store misses:                  "<< store_m << endl;
  cout << "Total misses:                  "<< total_m << endl;
  cout << "Load hits:                     "<< load_h << endl;
  cout << "Store hits:                    "<< store_h << endl;
  cout << "Total hits:                    "<< total_h << endl;
  cout << "_______________________________________" << endl;

  exit(0);
}

int main(int argc, char * argv []) {
  
  int size, asociativity, block_size, rp;

  // Obtencion de parametros de entrada
  for(int i = 0; i < argc; i++){ 
    if(string(argv[i]) == "-t"){ 
      size = atoi(argv[i+1]); 
    }
    else if(string(argv[i]) == "-l"){
      block_size = atoi(argv[i+1]);
    }
    else if(string(argv[i]) == "-a"){
      asociativity = atoi(argv[i+1]);
    }
    else if(string(argv[i]) == "-rp"){
      rp = atoi(argv[i+1]);
    }
  }

  // Se obtiene tag, index y offset 
  struct cache_params cache_param;
  cache_param.size = size;
  cache_param.block_size = block_size;
  cache_param.asociativity = asociativity;
  struct cache_field_size field_s;
  field_size_get(cache_param, &field_s);

  // Se crea el cache
  entry **cache;
  switch (rp)
  {
  case LRU:
    cache = cache_lru(asociativity, field_s.idx, rp);
    break;
  case NRU:
    cache = cache_nru(asociativity, field_s.idx, rp);
    break;
  case RRIP:
    cache = cache_srrip(asociativity, field_s.idx, rp);
    break;
  default:
    break;
  }
  
  // Variables necesarias para recorrer el trace
  struct operation_result res;
  int loadstore, idx, tag;
  int debug = 0;
  char line[100];
  char address_in[10];
  long address;
  float load_h = 0;
  float store_h = 0;
  float load_m = 0;
  float store_m = 0;
  int dirty_evic = 0;

  // Se recorre el trace
  while(fgets(line, 100, stdin)!= NULL){
    if(line[0] == 35){                                                                                 /* if line is valid */
      sscanf(line, "%*s %d %s", &loadstore, address_in);
      address = strtol(address_in, NULL, 16);
    }
    else{
      break;
    }
    address_tag_idx_get(address, field_s, &idx, &tag);
    switch (rp)
    {
    case LRU:
      lru_replacement_policy(idx, tag, asociativity, loadstore, cache[idx], &res, debug);
      break;
    case 1:
      nru_replacement_policy(idx, tag, asociativity, loadstore, cache[idx], &res, debug);
      break;
    case RRIP:
      srrip_replacement_policy(idx, tag, asociativity, loadstore, cache[idx], &res, debug);
      break; 
    default:
      lru_replacement_policy(idx, tag, asociativity, loadstore, cache[idx], &res, debug);
      break;
    }

    switch (res.miss_hit)
    {
    case HIT_STORE:
      store_h++;
      break;
    case MISS_STORE:
      store_m++;
      break;
    case HIT_LOAD:
      load_h++;
      break;
    case MISS_LOAD:
      load_m++;
      break;
    default:
      break;
    }
    if (res.dirty_eviction == true){
      dirty_evic++;
    }
  }

  // Impresion de resultados
  print_usage(size, asociativity, block_size, rp, load_h, load_m, store_h, store_m, dirty_evic);
  
return 0;
}