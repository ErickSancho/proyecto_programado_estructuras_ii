/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>


#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
   // Revision de parametros
   if(cache_params.size <= 0 || cache_params.asociativity <= 0 || cache_params.block_size <= 0){
      return ERROR;
   }
   field_size->offset = log2(cache_params.block_size);
   field_size->idx = log2(cache_params.size*KB/(cache_params.block_size*cache_params.asociativity));
   field_size->tag = ADDRSIZE - field_size->idx - field_size->offset;
   return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
   // tag
   long masc = field_size.idx + field_size.offset;
   long tag_s = address >> masc;
   *tag = (int)(tag_s);
   // idx
   long index_s = address;
   index_s = index_s << field_size.tag;
   long mask_b = 0xFFFFFFFF;
   index_s = index_s & mask_b;
   index_s = index_s >> field_size.tag + field_size.offset;
   *idx = (int)(index_s);
}


// Estructuras de cache para cada politica de reemplazo

entry **cache_lru(int ways, int idx_size, int rp){

   // Sets totales
   int set = pow(2, idx_size);
   entry **cache = new entry*[set];
   for(int i = 0; i < set; i++){
      cache[i] = new entry[ways];
   }
   // Se recorre los sets de la cache y se ponen valores iniciales
   for(int i = 0; i < set; i++){
     for(int j = 0; j < ways; j++){
      cache[i][j].valid = false;
      cache[i][j].dirty = false;
      cache[i][j].tag = 0;
      cache[i][j].rp_value = j;
      cache[i][j].obl_tag = false;
     }
   }
   return cache;
}

entry **cache_nru(int ways, int idx_size, int rp){
   // Sets totales
   int set = pow(2, idx_size);
   entry **cache = new entry*[set];
   for(int i = 0; i < set; i++){
      cache[i] = new entry[ways];
   }
   // Se recorre los sets de la cache y se ponen valores iniciales
   for(int i = 0; i < set; i++){
     for(int j = 0; j < ways; j++){
      cache[i][j].valid = false;
      cache[i][j].dirty = false;
      cache[i][j].tag = 0;
      cache[i][j].rp_value = 1;
      cache[i][j].obl_tag = false;
     }
   }
   return cache;
} 
entry **cache_srrip(int ways, int idx_size, int rp){
   // Sets totales
   int set = pow(2, idx_size);
   entry **cache = new entry*[set];
   for(int i = 0; i < set; i++){
      cache[i] = new entry[ways];
   }
   // Se recorre los sets de la cache y se ponen valores iniciales
   int M = 0;
   for(int i = 0; i < set; i++){
      for(int j = 0; j < ways; j++){
         cache[i][j].valid = false;
         cache[i][j].dirty = false;
         cache[i][j].tag = 0;
         cache[i][j].obl_tag = false;
         if(ways <= 2){
            M = 1;  
         }
         else{
            M = 2;
         }
         cache[i][j].rp_value = pow(2, M) - 1;
      }     
   }
   return cache;
}


int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{  

   // Revision de parametros
   if(idx < 0 || tag < 0 || associativity < 1){
      return PARAM;
   }
   int M = 0;
   if(associativity <= 2){
      M = 1;
   }
   else{
      M = 2;
   }

   int l_srrip = pow(2, M) - 2;
   int distant = pow(2, M) - 1;
   bool hit = false; 

   // Hay hit?
   for(int i = 0; i < associativity; i++){
      if(cache_blocks[i].tag == tag && cache_blocks[i].valid == 1){
         hit = true;
         cache_blocks[i].rp_value = 0;
         result->dirty_eviction = false;
         
         // Revision por si es un hit_store o hit_load
         if(loadstore == STORE){
            result->miss_hit = HIT_STORE;
            cache_blocks[i].dirty = true;
         }
         else{
            result->miss_hit = HIT_LOAD;
         }  
         return OK;
         
      }
   }

   // Si no hubo hit es un miss
   if(!hit){
      bool first_d = false;
      int way = 0;

      for(int i = 0; i < associativity; i++){
         // Revisa por el primer bloque que tenga rp_value = 2^M-1
         if(cache_blocks[i].rp_value == distant){
            first_d = true;

            if(cache_blocks[i].dirty == true){
               result->dirty_eviction = true;
               // Si el bit de dirty es 1 se hace un dirty_eviction
               result->evicted_address = cache_blocks[i].tag;
            }
            else{
               result->dirty_eviction = false;
            }
            
            // Se actualiza el bloque
            cache_blocks[i].tag = tag;
            cache_blocks[i].valid = true;
            cache_blocks[i].rp_value = l_srrip;

            // Revision por si es un hit_store o hit_load
            if(loadstore == STORE){
               cache_blocks[i].dirty = true;
               result->miss_hit = MISS_STORE;
            }
            else{
               cache_blocks[i].dirty = false;
               result->miss_hit = MISS_LOAD;
            }
            return OK;
         }
      }

      // Si no se encontro un bloque con rp_value = 2^M-1, se incrementa
      // el rp_value de todos los bloques hasta que haya un primer rp_value = 2^M-1
      while(first_d == false){
         for(int i = 0; i < associativity; i++){
            cache_blocks[i].rp_value++;
         }
         for(int i = 0; i < associativity; i++){
            if(cache_blocks[i].rp_value == distant){
               first_d = true;
               way = i;
               break;
            }
         }
      }

      if(cache_blocks[way].dirty == true){
         // Si el bit de dirty es 1 se hace un dirty_eviction
         result->dirty_eviction = true;
      }
      else{
         result->dirty_eviction = false;
      }
      result->evicted_address = cache_blocks[way].tag;

      // Se actualiza el bloque
      cache_blocks[way].tag = tag;
      cache_blocks[way].valid = true;
      cache_blocks[way].rp_value = l_srrip;
         
      // Revision por si es un hit_store o his_load
      if(loadstore == STORE){
         cache_blocks[way].dirty = true;
         result->miss_hit = MISS_STORE;
      }
      else{
         cache_blocks[way].dirty = false;
         result->miss_hit = MISS_LOAD;
      }
         return OK;
      }
   return ERROR;
}


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   // Revision de parametros
   if(idx < 0 || tag < 0 || associativity < 1){
      return PARAM;
   }

   bool flag_hit = false; //Bandera que me indica si hubo hit

   /*Analizo el caso de HIT*/
   for(int i = 0; i < associativity; i++)
   {
      if(cache_blocks[i].tag == tag && cache_blocks[i].valid == true)
      {
         //actualizo la bandera de hit
         flag_hit = true;

         //No eviction
         result->dirty_eviction = false;

         //Actualizo el valor del miss_hit de los resultados
         result->miss_hit = (loadstore) ? HIT_STORE : HIT_LOAD;

         //Actualizo el bit de dirty
         cache_blocks[i].dirty = (loadstore) ? true : cache_blocks[i].dirty;

         //Cambio de los valores de control de los bloques que tienen valor mayor que el bloque en cuestion
         for(int j = 0; j < associativity; j++){
               if(cache_blocks[j].rp_value > cache_blocks[i].rp_value)
               {
                  cache_blocks[j].rp_value--;
               }
            }
         //Le cambio el valor de control al mas alto, el cual corresponde al MRU
         cache_blocks[i].rp_value = associativity-1;
         
         break;
      }
   }

   //Caso de Miss
   if (!flag_hit)
   {
      for(int i = 0; i < associativity; i++)
      {
         if(cache_blocks[i].rp_value == 0)
         {
            //Pregunto si el dato esta sucio
            result->dirty_eviction = cache_blocks[i].dirty;

            //direccion del evicted
            result->evicted_address = cache_blocks[i].tag;

            //si es un load o store
            result->miss_hit = (loadstore) ? MISS_STORE : MISS_LOAD;
            cache_blocks[i].dirty = loadstore;

            //Reordenar el rp_value
            for(int j = 0; j < associativity; j++){
               if(cache_blocks[j].rp_value > cache_blocks[i].rp_value)
               {
                  cache_blocks[j].rp_value--;
               }
            }

            //Actualizacion del valor del rp_value
            cache_blocks[i].rp_value = associativity - 1; 

            //Carga de los datos de tag y valid
            cache_blocks[i].tag = tag;
            cache_blocks[i].valid = true;

            break;
         }
         
      }
   }


   return OK;
}

int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* result,
                           bool debug)
{

   // Revision de parametros
   if(idx < 0 || tag < 0 || associativity < 1){
      return PARAM;
   }

   bool hit = false;
   int way = 0;
   // Se recorren bloques en la asociatividad
   for(int i = 0; i < associativity; i++){
      if (cache_blocks[i].tag == tag && cache_blocks[i].valid == 1){ // cache hit
         hit = true;
         way = i;
      }   
   }

   // Si hay un hit
   if(hit == true){
      
      // Se pone el bloque con un rp = 0
      cache_blocks[way].rp_value = 0;
      result->dirty_eviction = false;

      // Revision por si es un hit_store o his_load
      if(loadstore == STORE){
         result->miss_hit = HIT_STORE;
         cache_blocks[way].dirty = 1;
      }
      else{
         result->miss_hit = HIT_LOAD;
      }  
   }
   // Revisa si hay un miss, lo cual quiere decir que anteriormente no hubo un hit 
   if(hit == false){
      bool first_1 = 0;
      way = 0;

      // Se busca el first_1 (rp_value = 1)
      for (int i = 0; i < associativity; i++){
         if (cache_blocks[i].rp_value == 1 && first_1 == 0){
            first_1 = 1;
            way = i;
         }
      }
      // Se encontró el primer uno?
      if(first_1 == 1){

         // Si el bit de dirty es 1 se hace un dirty_eviction
         if(cache_blocks[way].dirty == true){
            result->dirty_eviction = true;
         }
         else{
            result->dirty_eviction = false;
         }
         result->evicted_address = cache_blocks[way].tag;
         // Se actualiza el bloque
         cache_blocks[way].rp_value = 0;
         cache_blocks[way].tag = tag;
         cache_blocks[way].valid = 1;

         // Revisa si es un miss store o miss load
         if(loadstore == STORE){
            result->miss_hit = MISS_STORE;
            cache_blocks[way].dirty = 1;
         }
         else
         {  
            result->miss_hit = MISS_LOAD;
            cache_blocks[way].dirty = 0;
         }        
      }

      // Si no se encuentra el primer uno
      if(first_1 == 0){
         // Se pone 1 a todos los rp_value
         for (int i = 0; i < associativity; i++){
            cache_blocks[i].rp_value = 1;
         }

         // Y se vuelve a buscar el primer uno a la izquierda, el cual se sabe que esta en la primera posicion del set
            if(cache_blocks[0].dirty == true){
               // Si el bit de dirty es 1 se hace un dirty_eviction
               result->dirty_eviction = true;
            }
            else{
               result->dirty_eviction = false;
            }
            result->evicted_address = cache_blocks[way].tag;
            // Se actualiza el bloque
            cache_blocks[0].rp_value = 0;
            cache_blocks[0].tag = tag;
            cache_blocks[0].valid = 1;

            if(loadstore == STORE){
               result->miss_hit = MISS_STORE;
               cache_blocks[0].dirty = 1;
            }
            else
            {  
               result->miss_hit = MISS_LOAD;
               cache_blocks[0].dirty = 0;

            }        
         }
     }
         
   return OK;
}