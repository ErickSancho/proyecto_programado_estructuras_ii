/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	 bool loadstore,
       	                    	 entry* l1_cache_blocks,
       	                  	 	 entry* vc_cache_blocks,
        	                 	 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	         	 bool debug)
{
	// Comienza por verificar si hay hit en L1 y en el VC	

	// Busco Hit en el L1
	bool hit_L1=false;

	// Se realiza el cambio del valor en el L1, con politica LRU
	lru_replacement_policy(l1_vc_info->l1_idx, 
						l1_vc_info->l1_tag,
						l1_vc_info->l1_assoc, 
						loadstore, 
						l1_cache_blocks, 
						l1_result, 
						debug);
	if (l1_result->miss_hit == HIT_LOAD || l1_result->miss_hit == HIT_STORE)
	{
		hit_L1 == true;
	}
	

	// Busco si hay hit en el VC
	bool hit_VC=false;

	for (int i = 0; i < l1_vc_info->vc_assoc; i++)
	{
		if (l1_vc_info->l1_tag == vc_cache_blocks[i].tag)
		{
			hit_VC = true;
		}
	}

	// Una vez teniendo los hit en los bloques, se procede a realizar los cambios para todos los casos.
	
	/*	Para el caso de hit en L1, las acciones ya se realizan empleando la funcion "lru_replacement_policy",
	*	entonces se debe comenzar por la etapa de VC, lo cual solo se da cuando hay miss en L1.
	*/

	// Caso de hit en L1
	if(hit_L1==true)
	{
		
		return OK;
	}

	// Caso de miss en L1 y hit en VC
	if(hit_L1==false && hit_VC==true)
	{
		// Cargo el tipo de miss_hit en los resultados del VC
		miss_hit_status hit_VC = (loadstore) ? HIT_STORE : HIT_LOAD;
		vc_result->miss_hit = hit_VC;

		//Para el VC, Empleamos la politica FIFO:
		FIFO_replacement_policy_VC(vc_cache_blocks, 
									l1_result->evicted_address, 
									l1_vc_info->vc_assoc, 
									l1_result->dirty_eviction,
									vc_result);

		
		
		return OK;
	}

	// Caso de miss en L1 y miss en VC
	if(hit_L1==false && hit_VC==false)
	{	

		// Cargo el tipo de miss_hit en los resultados del VC
		miss_hit_status miss_VC = (loadstore) ? MISS_STORE : MISS_LOAD;
		vc_result->miss_hit = miss_VC;

		//Para el VC, Empleamos la politica FIFO:
		FIFO_replacement_policy_VC(vc_cache_blocks, 
									l1_result->evicted_address, 
									l1_vc_info->vc_assoc, 
									l1_result->dirty_eviction,
									vc_result);


		return OK;	
	}
   	return ERROR;
}

//////////////////////////////////////////////////////////////////////////

int FIFO_replacement_policy_VC(entry* vc_cache_blocks,
								int tag,
								int asociativity,
								bool dirty,
								operation_result* vc_result)
{
	//  Resultados de; VC
	if(vc_cache_blocks[asociativity-1].dirty == true){
		vc_result->dirty_eviction = true;
	}
	else{
		vc_result->dirty_eviction = false;
	}

	if(vc_cache_blocks[asociativity -1].valid =1){
		vc_result -> evicted_address = vc_cache_blocks[asociativity-1].tag;
	}

	/*Muevo los valores del VC*/
	for (int i = asociativity-1; i > asociativity-1; i++)
	{
		vc_cache_blocks[i].tag = vc_cache_blocks[i-1].tag;
		vc_cache_blocks[i].valid = vc_cache_blocks[i-1].valid;
		vc_cache_blocks[i].dirty = vc_cache_blocks[i-1].dirty;
	}
	
	vc_cache_blocks[0].tag = tag;
	vc_cache_blocks[0].valid = true;
	vc_cache_blocks[0].dirty = dirty;

	return OK;	
}
